import sys
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime
import multiprocessing as mp

plt.style.use('default')

y_size = 6
gn = 1.618
fs = 15
cmap = plt.cm.viridis
dpi = 1200

colors = [
    '#377eb8',
    '#3f8e3d',
    '#ff7f00',
    '#e41a1c',
    '#984ea3',
    '#ffff33',
    '#a65628',
    '#f781bf']

colors_pnl = [
    '#377eb8',
    '#984ea3',
    '#ff7f00',
    '#e41a1c',
    '#3f8e3d',
    '#ffff33',
    '#a65628',
    '#f781bf']

path_home = os.path.expanduser('~/Documents/ArrowReportBacktest/')

sl_vec_default = [np.inf] 
proba_success_default = 50 
remove_big_days_default = 0
threshold_secs_default = 0 
score_min_default = 0
score_max_default = np.inf
max_trades_default = np.inf
max_net_trades_default = np.inf

class RunPostAnomalyAnalysis():
    def __init__(self, 
            directory, 
            session, 
            holding_period,
            sl_vec=[np.inf], 
            proba_success_vec=[50],
            threshold_secs_vec = [0],
            max_trades_vec=[np.inf],
            remove_big_days=0,
            threshold_secs=0,
            max_trades=np.inf,
            max_net_trades=np.inf,
            score_min=0,
            score_max=100,
            year_min=2000,
            stocks=None,
            save=False):
        self.directory = directory
        if self.directory[-1] != '/':
            self.directory += '/'
        self.holding_period = holding_period
        self.session = session
        self.proba_success_vec = proba_success_vec
        self.threshold_secs_vec = threshold_secs_vec
        self.threshold_secs = threshold_secs
        self.score_min = score_min
        self.score_max = score_max
        self.year_min = year_min
        self.instrument = directory.split('/')[0]
        self.sl_vec = sl_vec
        self.remove_big_days = remove_big_days
        self.max_trades = max_trades
        self.max_net_trades = max_net_trades
        self.n_trades_dic = {}
        print('max_trades', self.max_trades)
        self.max_trades_vec = max_trades_vec
        self.stocks = stocks
        self.save = save

    def runPostAnomalyAnalysis(self, proba_success):
        print('proba_success', proba_success)
        analysis = PostAnomalyAnalysis(
                directory=self.directory,
                holding_period=self.holding_period,
                session=self.session, 
                save=self.save)
        analysis.setParameters(
                sl_vec=self.sl_vec, 
                proba_success=proba_success, 
                remove_big_days=self.remove_big_days,
                threshold_secs=self.threshold_secs, 
                score_min=self.score_min,
                score_max=self.score_max,
                year_min=self.year_min,
                max_trades=self.max_trades,
                max_net_trades=self.max_net_trades)
        if self.stocks is not None:
           analysis.selectStocks(self.stocks)
        analysis.filterProbaSuccess()  # df_price
        analysis.filterScore()  # df_price
        analysis.filterExclusionWindow()  # df_price
        analysis.getTradesStopLoss()  # df_price -> df_price_dic
        analysis.filterCounterAnomaly()  # df_price_dic
        analysis.filterOverlappingTrades()  # df_price_dic
        analysis.getReturns()  # df_price_dic -> ret_dic
        analysis.getDailyReturns()  # ret_dic -> ret_daily_dic
        analysis.pltMultiplePNL()
        analysis.pltMultiplePersistence()
        analysis.pltQuartilePerformance()
        analysis.getReports()

    def runPostAnomalyAnalysisThresholdSec(self, threshold_secs):
        print('proba_success', self.proba_success)
        print('threshold_secs', threshold_secs)
        print('runPostAnomalyAnalysisThresholdSec max_trades', self.max_trades)
        analysis = PostAnomalyAnalysis(
                directory=self.directory,
                holding_period=self.holding_period,
                session=self.session, 
                save=self.save)
        analysis.setParameters(
                sl_vec=self.sl_vec, 
                proba_success=self.proba_success, 
                remove_big_days=self.remove_big_days,
                threshold_secs=threshold_secs, 
                score_min=self.score_min,
                score_max=self.score_max,
                year_min=self.year_min,
                max_trades=self.max_trades)
        analysis.filterProbaSuccess()  # df_price
        analysis.filterScore()  # df_price
        analysis.filterExclusionWindow()  # df_price
        analysis.getTradesStopLoss()  # df_price -> df_price_dic
        analysis.filterCounterAnomaly()  # df_price_dic
        analysis.filterOverlappingTrades()  # df_price_dic
        analysis.getReturns()  # df_price_dic -> ret_dic
        analysis.getDailyReturns()  # ret_dic -> ret_daily_dic
        analysis.pltMultiplePNL()
        analysis.pltMultiplePersistence()
        analysis.pltQuartilePerformance()
        analysis.getReports()

    def runPostAnomalyAnalysisOverlappingTrades(self, max_trades):
        print('max_trades', max_trades)
        analysis = PostAnomalyAnalysis(
                directory=self.directory,
                holding_period=self.holding_period,
                session=self.session, 
                save=self.save)
        analysis.setParameters(
                sl_vec=self.sl_vec, 
                proba_success=self.proba_success, 
                remove_big_days=self.remove_big_days,
                threshold_secs=self.threshold_secs, 
                score_min=self.score_min,
                score_max=self.score_max,
                year_min=self.year_min,
                max_trades=max_trades)
        analysis.filterProbaSuccess()  # df_price
        analysis.filterScore()  # df_price
        analysis.filterExclusionWindow()  # df_price
        analysis.getTradesStopLoss()  # df_price -> df_price_dic
        analysis.filterCounterAnomaly()  # df_price_dic
        analysis.filterOverlappingTrades()  # df_price_dic
        analysis.getReturns()  # df_price_dic -> ret_dic
        analysis.getDailyReturns()  # ret_dic -> ret_daily_dic
        analysis.pltMultiplePNL()
        analysis.pltMultiplePersistence()
        analysis.pltQuartilePerformance()
        analysis.getReports()

    def getSummary(self):
        report_main = pd.DataFrame(index=self.proba_success_vec, 
                                   columns=[['Trades', 'Acc(%)', 'Ret(BP)', 
                                             'RetTotal(%)', 'Sharpe', 'IR']])
        report_main.index.name = 'ProbabilitySuccess'
        print(path_home + self.directory+ 'bigDaysRemoved' + str(self.remove_big_days) + '/')
        for proba_success in self.proba_success_vec:
            directory_ = path_home + self.directory+ 'bigDaysRemoved' + str(self.remove_big_days) + '/'
            report_trade = pd.read_csv(directory_+'/reportTrade' + self.session+str(proba_success) + '.csv')
            report_trade.iloc[0][['Acc(%)', 'Mean(BP)']]

            report_daily = pd.read_csv(directory_+'reportDaily' + self.session+ str(proba_success) + '.csv')
            report_main.loc[proba_success, 'Trades'] = report_daily.iloc[0]['Trades']
            report_main.loc[proba_success, 'Acc(%)'] = report_trade.iloc[0]['Acc(%)']
            report_main.loc[proba_success, 'Ret(BP)'] = report_trade.iloc[0]['Ret(BP)']
            report_main.loc[proba_success, 'RetTotal(%)'] = report_daily.iloc[0]['RetTotal(%)']
            report_main.loc[proba_success, 'Sharpe'] = report_daily.iloc[0]['Sharpe']
            report_main.loc[proba_success, 'IR'] = report_daily.iloc[0]['IR']
        if self.save:
            report_main.to_csv(directory_ +'/report'+self.session+'.csv')
            report_main.to_html(directory_ + '/report'+self.session+'.html')

    def runPostAnomalyAnalysisThresholdSecParallel(self):
        print('runPostAnomalyAnalysisThresholdSecParallel')
        for proba_success in self.proba_success_vec:
            self.proba_success = proba_success
            self.runPostAnomalyAnalysisThresholdSecParallelProba()

    def runPostAnomalyAnalysisOverlappingTradesParallel(self):
        for proba_success in self.proba_success_vec:
            self.proba_success = proba_success
            self.runPostAnomalyAnalysisOverlappingTradesParallelProba()

    def runPostAnomalyAnalysisParallel(self):
        n_jobs = len(self.proba_success_vec)
        pool = mp.Pool(n_jobs)
        pool.map(self.runPostAnomalyAnalysis, self.proba_success_vec)
        pool.close()
        pool.join()

    def runPostAnomalyAnalysisOverlappingTradesParallelProba(self):
        n_jobs = len(self.max_trades_vec)
        pool = mp.Pool(n_jobs)
        pool.map(self.runPostAnomalyAnalysisOverlappingTrades, self.max_trades_vec)
        pool.close()
        pool.join()

    def runPostAnomalyAnalysisThresholdSecParallelProba(self):
        print('runPostAnomalyAnalysisThresholdSecParallelProba')
        n_jobs = len(self.threshold_secs_vec)
        pool = mp.Pool(n_jobs)
        pool.map(self.runPostAnomalyAnalysisThresholdSec, self.threshold_secs_vec)
        pool.close()
        pool.join()


class PostAnomalyAnalysis():
    def __init__(self, 
                directory, 
                holding_period,
                session, 
                save=False):
        self.directory_ = directory
        self.holding_period = holding_period
        self.session = session
        self.instrument = directory.split('/')[0]
        self.save = save
        self.df_res_dic = {}
        self.returns_dic = {}
        self.returns_daily_dic = {}
        self.returns_stock_dic = {}
        self.report_trade = pd.DataFrame([])
        self.report_daily = pd.DataFrame([])
        self.ret_mat = np.array([])
        self.filters = False

    def setParameters(self, 
            sl_vec=sl_vec_default, 
            proba_success=proba_success_default, 
            remove_big_days=0,
            threshold_secs=0, 
            score_min=0,
            score_max=np.inf,
            max_trades=np.inf,
            max_net_trades=np.inf,
            year_min=2000,
            sl_max=50):
        self.sl_vec = sl_vec
        self.proba_success = proba_success
        self.remove_big_days = remove_big_days
        self.threshold_secs = threshold_secs
        self.score_min = score_min
        self.score_max = score_max
        self.max_trades = max_trades
        self.max_net_trades = max_net_trades
        self.sl_max = sl_max
        self.df_price_dic = {}
        self.directory = path_home + self.directory_ + 'bigDaysRemoved' + str(remove_big_days)
        if threshold_secs:
            self.directory += 'ThresholdSec' + str(threshold_secs)
        if max_trades<10000:
            self.directory += 'MaxTrades' + str(max_trades)
        if max_net_trades<10000:
            self.directory += 'MaxNetTrades' + str(max_net_trades)
        self.directory += '/'
        os.makedirs(self.directory, exist_ok=True)
        self.end_file_name = self.session + str(self.proba_success)
        if self.directory_[-1] != '/':
            self.directory_ += '/'
        path_vwap_file = path_home + self.directory_ + 'vwaps' + self.session + '.csv'
        self.df_price = pd.read_csv(path_vwap_file, parse_dates=['DateTime'])
        self.df_price = self.df_price[self.df_price['DateTime'].dt.year >= year_min]
        if self.instrument == 'TokyoTick':
            self.df_price = self.df_price[self.df_price['DateTime'].dt.date != datetime.date(2017, 7, 17)]
        self.df_price = self.df_price[self.df_price.columns[:self.holding_period+4]]
        self.cols = list(self.df_price.columns[7: self.holding_period+4])
        self.n_buckets = len(self.cols)
        self.df_price.sort_values(['Stock', 'DateTime'], inplace=True)
        self.df_price.set_index(np.arange(self.df_price.shape[0]), inplace=True)
        self.remove_big_days = remove_big_days
        if remove_big_days:
            self.df_price['Date'] = self.df_price['DateTime'].dt.date
            date_count = self.df_price[['Date', self.cols[0]]].groupby('Date').count()
            date_count.sort_values(by=self.cols[0], ascending=False, inplace=True)
            mask = np.ones(len(self.df_price.index))
            for date in date_count.index[:remove_big_days]:
                mask[self.df_price['Date']==date] = 0
            self.df_price = self.df_price[mask.astype(bool)]
        self.df_price = self.df_price[self.df_price[self.cols[0]] != 0]
        self.df_price = self._cleanPriceDataFrame(self.df_price)
        self.df_price['CloseTime'] = self.df_price['DateTime'] + datetime.timedelta(minutes=self.holding_period)
        self.df_price['CloseTimeStoploss'] = self.df_price['CloseTime']
        self.df_price['CloseTimeCounterAnomaly'] = self.df_price['CloseTime']
        # self.df_price['ReasonClosed'] = '0'
        # print(self.df_price.head(2))
        self.df_price['Date'] = self.df_price['DateTime'].dt.date
        self.df_price.sort_values(['DateTime'], inplace=True)
        self.df_price_backup = self.df_price.copy()        
        self.df_price_filtered = self.df_price[0:1].copy()    
        self.n_trades_dic = {}

    def selectStocks(self, stocks):
        mask = [i in stocks for i in self.df_price['Stock'].values] 
        # print(mask)
        self.df_price = self.df_price[mask]
        # print(self.df_price['DateTime'].dt.date)
        self.df_price = self._cleanPriceDataFrame(self.df_price)

    def getCloseTime(self):
        for sl in self.sl_vec: 
            diff = self.df_price_dic[sl][self.cols].diff(periods=1, axis=1)
            self.df_price_dic[sl]['CloseTime'] = self.df_price_dic[sl]['DateTime'].copy()
            for k in range(len(self.df_price_dic[sl])):
                minutes_close = 0
                for col, i in zip(np.array(self.cols[::][::-1]), np.ones(len(self.cols))):
                    if diff.iloc[k][col] != 0:
                        break
                    minutes_close += i
                self.df_price_dic[sl]['CloseTime'].iloc[k] = self.df_price_dic[sl]['DateTime'].iloc[k] + \
                    datetime.timedelta(minutes=self.holding_period-minutes_close)
    
    def _cleanPriceDataFrame(self, df_price):
        self.n_trades = df_price.shape[0]
        df_price.index = np.arange(self.n_trades)
        df_price = df_price.fillna(method='ffill', axis=1)
        df_price = df_price[df_price[self.cols[0]] != 0]
        self.dates = list(pd.unique(df_price['DateTime'].dt.date))
        self.dates.sort()
        self.stocks = pd.unique(df_price['Stock'])
        self.stocks.sort()
        return df_price

    def applyDailyCumSum(self, gr, portfolio_sl):
        columns = gr.index.round(freq='1min')
        index = pd.date_range("9:00", "11:30", freq="1min").time
        cols = np.arange(len(columns))
        daily_df = pd.DataFrame(columns=cols, index=index)
        daily_df.loc[:] = 0
        for i in cols:
            time_start = columns[i] + datetime.timedelta(minutes=4)
            time_end = time_start + datetime.timedelta(minutes=25)
            time_start = time_start.time()
            time_end = time_end.time()
            daily_df.loc[time_start:time_end, cols[i]] += gr[self.cols[1:]].iloc[i].values
            daily_df.loc[time_end:, cols[i]] += gr[self.cols[-1]].iloc[i]
        daily_sum = daily_df.sum(axis=1)
        ret_min = daily_sum.min() / 100
        ret_max = daily_sum.max() / 100
        ret = daily_sum[-1] / 100
        n_trades = len(columns)
        if ret_min < portfolio_sl:
            time_cut = daily_sum.index[daily_sum<=portfolio_sl][0]
            ret_cut = daily_sum.loc[time_cut]
            n_trades_cut = (columns.time < time_cut).sum()
        else:
            n_trades_cut = n_trades
            ret_cut = ret
        
        return pd.Series({'Ret': ret,
                        'RetMin': ret_min, 
                        'Trades': n_trades,
                        'RetMax': ret_max, 
                        'RetCut': ret_cut,
                        'TradesCut': n_trades_cut})
    
    def getPorfolioStopLoss(self, portfolio_sl):
        portfolio_sl = - np.abs(portfolio_sl)
        self.df_portfolio_sl = {}
        for sl in self.sl_vec:
            res = self.ret_dic[sl].groupby('Date').apply(applyDailyCumSum, portfolio_sl=portfolio_sl)
            res.index = pd.to_datetime(res.index)
            self.df_portfolio_sl[sl] = res

    def filterProbaSuccess(self, shift_buy=0, shift_sell=0):
        if self.proba_success == proba_success_default and (shift_buy == 0) and (shift_sell == 0): 
            return
        proba_sucess = self.proba_success / 100
        shift_buy /= 100
        shift_sell /= 100
        self.df_price = self.df_price[((self.df_price['ProbaSuccess']>proba_sucess+shift_buy) & 
                                       (self.df_price['Action']==1)) |
                                      ((self.df_price['ProbaSuccess']>proba_sucess+shift_sell) &
                                       (self.df_price['Action']==-1))]
        self.df_price = self._cleanPriceDataFrame(self.df_price)


    def checkFilterExclusionWindowActionDependent(self, trade):
        '''
        This filter works exactly as checkFilterExclusionWindowActionIndependent, with only
        one difference: the anomaly passes the filter if it is more than self.threshold_secs away
        from the opening time of the last same directional trade for the particular stock.
        
        Parameters
        ----------
        trade : pd.Series
            A pandas series containing the full information for a specific anomaly as described in
            applyFilters function.
                
        Returns
        ----------
        passFilter : boolean
            True if the anomaly passes the filter.
        '''
    
        stock = trade['Stock']
        action = trade['Action']
        open_time = trade['DateTime'] 
        current_day = trade['Date']
        passFilter = True
        mask = ((self.df_price_filtered['Stock'] == stock)
                & (self.df_price_filtered['Action'] == action)
                & (self.df_price_filtered['Date'] == current_day))
        if mask.sum() > 0:
            last_open_time = self.df_price_filtered[mask].iloc[-1]['DateTime']
            if open_time - last_open_time < datetime.timedelta(seconds=self.threshold_secs):
                passFilter = False
        return passFilter



    def checkFilterExclusionWindowActionIndependent(self, trade):
        '''
        Given an anomaly for a specific stock, this filter checks whether there the last trade 
        for this stock was opened less than a specific time before. This time window parameter 
        (in seconds) is stored in self.threshold_secs. If the last trade was opened less than 
        self.threshold_secs seconds ago, the anomaly does not pass the filter.
        
        Parameters
        ----------
        trade : pd.Series
            A pandas series containing the full information for a specific anomaly as described in
            applyFilters function.
                
        Returns
        ----------
        passFilter : boolean
            True if the anomaly passes the filter.
        '''
        stock = trade['Stock']
        open_time = trade['DateTime'] 
        current_day = trade['Date']
        passFilter = True
        # mask the previous anomalies belonging to the current day and stock 
        # that have successfully passed the filters
        mask = ((self.df_price_filtered['Stock'] == stock)
                & (self.df_price_filtered['Date'] == current_day))
        if mask.sum() > 0:
            last_open_time = self.df_price_filtered[mask].iloc[-1]['DateTime']
            # check if the difference in time between the current anomaly time and the opening time
            # of the last trade exceeds self.threshold_secs 
            if open_time - last_open_time < datetime.timedelta(seconds=self.threshold_secs):
                passFilter = False
        return passFilter

    def checkFilterNetPosition(self, trade):
        '''
        The purpose of this filter is to keep a balanced number of opened buys and opened sells
        across the portfolio at a specific moment in time. The difference between the number
        of opened buys and sells at a moment in time is the net position of the portfolio. 
        The threshold on the net position is stored in self.max_net_trades. 
        Given a new anomaly, this filter calculates the current net position of the portfolio (before
        trading the new anomaly) and the new net position of the portfolio (if the new anomaly 
        would be traded). If the new net position is not larger than the threshold, the anomaly passes
        the filter. If the new net position is larger than the threshold, however it is smaller than
        the current net position (it decreases the imbalance between buys and sells), the anomaly passes
        the filter.

        Parameters
        ----------
        trade : pd.Series
            A pandas series containing the full information for a specific anomaly as described in
            applyFilters function.
                
        Returns
        ----------
        passFilter : boolean
            True if the anomaly passes the filter.
        '''
        action = trade['Action']
        current_day = trade['Date']
        # mask the anomalies of the current day
        mask = self.df_price_filtered['Date'] == current_day
        open_time = trade['DateTime'] 
        passFilter = True
        if mask.sum() > 0:
            close_times = self.df_price_filtered[mask]['CloseTime']
            # mask all the trades that are currently open at the anomaly time
            open_trades = (close_times > open_time)
            if open_trades.sum() > 0:
                # calculate the current net position and the new net position 
                current_net_position = np.abs(self.df_price_filtered[mask][open_trades]['Action'].sum())
                new_net_position = np.abs(self.df_price_filtered[mask][open_trades]['Action'].sum() + action)
                
                if new_net_position <= self.max_net_trades:
                    # if the new net position does not exceed the threshold, pass the trade
                    passFilter = True
                elif new_net_position < current_net_position:
                    # if the new net position exceeds the threshold but decreases the net position, pass the trade
                    passFilter = True
                else:
                    passFilter = False
        return passFilter

    def checkFilterOverlappingTrades(self, trade):
        '''
        The purpose of this filter is to not allow the number of trades in the same direction for 
        the same stock to exceed a specific threshold (stored in self.max_trades). 
        Given a new anomaly on a specific stock, this filter calculates the number of open trades
        on that stock in the same direction as the new anomaly. If this number is larger or equal 
        to self.max_trades, the new anomaly does not pass the filter. 

        Parameters
        ----------
        trade : pd.Series
            A pandas series containing the full information for a specific anomaly as described in
            applyFilters function.
                
        Returns
        ----------
        passFilter : boolean
            True if the anomaly passes the filter.
        '''
        stock = trade['Stock']
        action = trade['Action']
        open_time = trade['DateTime'] 
        current_day = trade['Date']
        passFilter = True
        mask = ((self.df_price_filtered['Stock'] == stock) & 
                (self.df_price_filtered['Action'] == action)
                & (self.df_price_filtered['Date'] == current_day))
        if mask.sum() > 0:
            close_times = self.df_price_filtered[mask]['CloseTime']
            if (close_times > open_time).sum() >= self.max_trades:
                passFilter = False
        return passFilter

    def checkCounterAnomaly(self, trade):
        '''
        The purpose of this filter is to not allow trades in opposite directions opened at the same time
        for the same stock. 
        Given a new anomaly on a specific stock, this rule checks whether there are open trades for this stock
        in the opposite direction of the new anomaly that are currently opened at the anomaly time. If yes,
        all these the opposite direction trades are closed immediately. Their corresponding closing times and
        vwap prices in self.df_price_filtered are modified accordingly.

        Parameters
        ----------
        trade : pd.Series
            A pandas series containing the full information for a specific anomaly as described in
            applyFilters function.
        '''
        # self.cols is the list with the column names for the vwaps ['vwap_2', 'vwap_5', 'vwap_6', ...]
        cols = self.cols
        stock = trade['Stock']
        action = trade['Action']
        open_time = trade['DateTime'] 
        current_day = trade['Date']
        # mask all the previous trades for the specific stock, specific day 
        # in the opposite direction with the anomaly
        mask = ((self.df_price_filtered['Stock'] == stock) & 
                (self.df_price_filtered['Action'] == -1*action)
                & (self.df_price_filtered['Date'] == current_day))
        
        if mask.sum() > 0:
            close_times = self.df_price_filtered[mask]['CloseTime']
            # mask opposite direction trades that are currently opened
            mask_close = close_times > open_time
            if mask_close.sum() > 0:
                close_index = self.df_price_filtered[mask][mask_close].index
                # modify the closing time of the opposite direction trades
                self.df_price_filtered.loc[close_index, 'CloseTime'] = open_time
                self.df_price_filtered.loc[close_index, 'CloseTimeCounterAnomaly'] = open_time
                # self.df_price_filtered.loc[close_index, 'ReasonClosed'] = 'Counter'
                # for each of these trades, find out for how long it has been opened
                difference_mins = (open_time -  
                                self.df_price_filtered.loc[close_index, 'DateTime']
                                ).dt.total_seconds()//60 + 1
                for idx, mins in zip(close_index, difference_mins):
                    mins = int(mins)
                    if mins <= 5:
                        # if the trade has been opened for 5 minutes or less set all vwaps form vwap_5
                        # equal to vwap_5
                        self.df_price_filtered.loc[idx, cols[1:]] = self.df_price_filtered.loc[idx, cols[1]] 
                    else:
                        # if the trade has been opened for more than 5 minutes, cols[mins-4:] is the vwap at which
                        # the trade is closed. All the values for the rest of the vwaps is set to the closing vwap.
                        self.df_price_filtered.loc[idx, cols[mins-4:]] = self.df_price_filtered.loc[idx, cols[mins-4]] 

    def applyStoploss(self, index, stoploss):
        '''
        The purpose of this filter is to not allow trades in opposite directions opened at the same time
        for the same stock. 
        Given a new anomaly on a specific stock, this rule checks whether there are open trades for this stock
        in the opposite direction of the new anomaly that are currently opened at the anomaly time. If yes,
        all these the opposite direction trades are closed immediately. Their corresponding closing times and
        vwap prices in self.df_price_filtered are modified accordingly.

        Parameters
        ----------
        index : integer
            The index of the current anomaly in self.df_price

        stoploss: integer
            The stoploss threshold between 0 and 100
        '''
        cols = self.cols
        # compute the return of the trade at each vwap bucket
        rets = 100 * 100 * (self.df_price[cols].loc[index].values / 
                            self.df_price[cols[0]].loc[index] - 1)

        # get the indices of the vwap buckets at which the return hits the stoploss threshold 
        indx = np.argwhere(rets < -stoploss)
        if indx.sum():
            # get the first vwap bucket which triggers the stoploss and close the trade at that vwap price
            # by setting the rest of the vwaps to the current one
            self.df_price.loc[index, cols[indx[0][0]+1:]] = self.df_price.loc[index][cols[indx[0][0]]]
            # modify the closing time of the trade 
            self.df_price.loc[index, 'CloseTime'] = self.df_price.loc[index, 'DateTime'] + datetime.timedelta(
                minutes=int(indx[0][0]+4))
            self.df_price.loc[index, 'CloseTimeStoploss'] = self.df_price.loc[index, 'DateTime'] \
                                                            + datetime.timedelta(
                                                                minutes=int(indx[0][0]+4))
            # self.df_price.loc[index, 'ReasonClosed'] = 'Stoploss' 
                
    def applyFilters(self, counter_anomaly=True, stoploss=None):
        '''
        Loop through all the anomalies and apply 3 filters and the 
        counter anomaly rule:
            1- Exclusion window filter
            2- Counter anomaly rule
            3- Net position filter
            4- Overlapping trades filter
        Filters 1, 3 and 4 check whether the anomaly will be traded or not.
        The counter anomaly rule checks whether other open trades need to be closed in order
        to open a new one. If the anomaly passes Filter 1, counter anomaly rule is applied to
        close the open trades that violate this rule. After that, the anomaly goes through
        Filter 3 & Filter 4. If it passes both filters, then the anomaly is allowed to be traded.

        For the anomalies that pass all the filters, then the stoploss rule is applied. 
        
        Each anomaly is a pandas series containing the following information for a specific anomaly:
            - DateTime: the anomaly time
            - Stock: the stock that triggered the anomaly
            - Action: the predicted price direction (1 or -1)                                           -1
            - DeltaPrice: the difference in price in bps between the opening price and
                            the price after self.holding_period minutes.                             
            - Score: the score given by the anomaly detector                                        
            - ProbaSuccess: the probability of success given by the classifier                                
            - AnomalyPrice: the closing price of the tick bar at which the anomaly occured                                   
            - Vwap prices: The first one is 2 minutes vwap 'vwap_2',
                            the rest are 5 minutes vwaps starting from 'vwap_5', 'vwap_6', 
                            up to e.g. 'vwap_30' if self.holding_period is 30 minutes.
            - CloseTime: the closing time of the trade              
            - CloseTimeStoploss: the closing time of the trade if the stoploss was trigered.
                                If not, it is equal to the 'CloseTime'.  
            - CloseTimeCounterAnomaly: the closing time of the trade if the counter anomaly rule 
                                        was trigered. If not, it is equal to the 'CloseTime'.

        All the filtered anomalies are stored in self.df_price_filtered with the corresponding 
        opening time, closing time, opening vwap price (vwap 2 minutes) and closing vwap price
        (vwap 5 minutes).
        
        
        Parameters
        ----------
        counter_anomaly : boolean
            If True, the counter anomaly rule is applied. Default value is True.

        stoploss : integer
            It is the loss in bps the trade needs to reach
            in order to be closed.
        '''
        # self.df_price is set to the original prices dataframe
        self.df_price = self.df_price_backup.copy()
        # self.df_price_filtered is initialised with the first anomaly of self.df_price.
        # Other anomalies will be added one by one.
        self.df_price_filtered = self.df_price[0:1].copy()   
        
        # Loop through all the anomalies
        for i, trade in self.df_price.iloc[1:].iterrows():
            print(trade)
            print(type(trade))
            
            # Exclusion Window
            if self.threshold_secs != threshold_secs_default:
                passFilterExclusionWindow = self.checkFilterExclusionWindowActionIndependent(trade)
                if not passFilterExclusionWindow:
                    continue

            # Counter Anomaly
            if counter_anomaly:
                self.checkCounterAnomaly(trade)

            # Net Position Filter
            if self.max_net_trades < 10000000:
                passFilterNetPosition = self.checkFilterNetPosition(trade)
                if not passFilterNetPosition:
                    continue

            # Overlapping Trades
            if self.max_trades < 10000000:
                passFilterOverlappingTrades = self.checkFilterOverlappingTrades(trade)
                if not passFilterOverlappingTrades:
                    continue

            # Stop Loss   
            if stoploss < 10000000:
                self.applyStoploss(i, stoploss)
            
            # If the anomaly passes all the filters, it is added to self.df_price_filtered
            self.df_price_filtered = self.df_price_filtered.append(self.df_price.loc[i])
        
        print(self.df_price_filtered.shape)

        # self.df_price_dic stores the filtered anomalies for different stoploss values
        self.df_price_dic[stoploss] = self.df_price_filtered
        # self.n_trades_dic stores the number of filtered anomalies for different stoploss values
        self.n_trades_dic[stoploss] = len(self.df_price_filtered)

    def getReturns(self):
        self.ret_dic = {}
        for sl in self.df_price_dic.keys():
            self.ret_dic[sl] = pd.DataFrame(0, 
                    index=self.df_price_dic[sl]['DateTime'], 
                    columns=['Date', 'Stock']+self.cols)
            self.ret_dic[sl][self.cols] = 100 * 100 * (self.df_price_dic[sl][self.cols].values / 
                    self.df_price_dic[sl][self.cols[0]].values.reshape(self.n_trades_dic[sl], 1) - 1) \
                    * self.df_price_dic[sl]['Action'].values.reshape(self.n_trades_dic[sl], 1)
            self.ret_dic[sl]['Stock'] = self.df_price_dic[sl]['Stock'].values
            self.ret_dic[sl]['Action'] = self.df_price_dic[sl]['Action'].values
            self.ret_dic[sl]['Date'] = self.df_price_dic[sl]['DateTime'].dt.date.values














    def getDailyReturns(self):
        self.ret_daily_dic = {}
        for sl in self.sl_vec:
            ret_daily = (self.ret_dic[sl][['Date', self.cols[-1]]].groupby('Date')).agg(['sum', 'count'])
            ret_daily.columns = ret_daily.columns.droplevel(0)
            ret_daily.columns = ['Ret(%)', 'Trades']
            ret_daily['Ret(%)'] /= 100 
            self.ret_daily_dic[sl] = ret_daily

    def getStockReturns(self):
        self.ret_stock_dic = {}
        for sl in self.sl_vec:
            ret_stock = self.ret_dic[sl][['Stock', self.cols[-1]]].groupby('Stock').agg(['mean', 'sum', 'count'])
            ret_stock.columns = ret_stock.columns.droplevel(0)
            ret_stock.columns = ['Ret(BP)', 'Ret(%)', 'Trades']
            ret_stock['Ret(%)'] /= 100  
            self.ret_stock_dic[sl] = ret_stock.sort_values('Ret(BP)', ascending=False)
            if self.save:
                self.ret_stock_dic[sl].to_csv(self.directory+'reportStock'+self.end_file_name+'_sl'+str(sl)+'.csv')
                self.ret_stock_dic[sl].to_html(self.directory+'reportStock'+self.end_file_name+'_sl'+str(sl)+'.html')

    def getReports(self):
        index = []
        for sl in self.sl_vec:
            if sl == np.inf:
                index.append('NO  ')
            else:
                index.append(str(sl)+'bp')
        columns = ['Acc(%)',  'Ret(BP)', 'RetStd(BP)', 'Long(%)', 'Short(%)',
                 'AccLong(%)', 'AccShort(%)']   
        self.report_trade = pd.DataFrame(columns=columns, index=index)
        self.report_trade.index.name = 'STOPLOSS'
        columns = ['Trades', 'Acc(%)', 'Ret(%)', 'RetStd(%)', 
                   'RetMin(%)', 'RetMax(%)', 'RetTotal(%)', 'Sharpe', 'IR']
        self.report_daily = pd.DataFrame(columns=columns, index=index)
        self.report_daily.index.name = 'STOPLOSS'
        # returns_dic = self.returns_dic
        
        for ind, sl in zip(index, self.sl_vec):
            ret = self.ret_dic[sl][self.cols[-1]].values
            ret_daily = self.ret_daily_dic[sl]
            Action = self.df_price_dic[sl]['Action']
            information_ratio = ret_daily['Ret(%)'].mean() / ret_daily['Ret(%)'].std()
            indx_positive = ret > 0
            indx_daily_positive = ret_daily['Ret(%)'] > 0
            ratio_long = (Action > 0).mean()
            ratio_short = (Action < 0).mean()
            ratio_positive_long =  ((ret > 0) & (Action > 0)).sum() / (Action > 0).sum()
            ratio_positive_short = ((ret > 0) & (Action < 0)).sum() / (Action < 0).sum()
            self.report_trade['Acc(%)'].loc[ind] = round(100*indx_positive.mean(), 2)
            self.report_trade['Long(%)'].loc[ind] = round(100*ratio_long, 2)
            self.report_trade['Short(%)'].loc[ind] = round(100*ratio_short, 2)
            self.report_trade['Ret(BP)'].loc[ind] = round(ret.mean(), 5)
            self.report_trade['RetStd(BP)'].loc[ind] = round(ret.std(), 2)

            self.report_trade['AccLong(%)'].loc[ind] = round(100*ratio_positive_long, 2)
            self.report_trade['AccShort(%)'].loc[ind] = round(100*ratio_positive_short, 2)
            self.report_daily['Trades'].loc[ind] = len(Action)
            self.report_daily['Acc(%)'].loc[ind] = round(100*indx_daily_positive.mean(), 2)
            self.report_daily['Ret(%)'].loc[ind] = round(ret_daily['Ret(%)'].mean(), 2)
            self.report_daily['RetStd(%)'].loc[ind] = round(ret_daily['Ret(%)'].std(), 2)
            self.report_daily['RetMin(%)'].loc[ind] = round(ret_daily['Ret(%)'].min(), 2)
            self.report_daily['RetMax(%)'].loc[ind] = round(ret_daily['Ret(%)'].max(), 2)
            self.report_daily['RetTotal(%)'].loc[ind] = int(ret_daily['Ret(%)'].sum())
            self.report_daily['Sharpe'].loc[ind] = round(np.sqrt(255)*information_ratio, 2)
            self.report_daily['IR'].loc[ind] = round(information_ratio, 2)
        if self.save:
            print(self.directory+'reportTrade'+self.end_file_name+'.csv')
            self.report_trade.to_csv(self.directory+'reportTrade'+self.end_file_name+'.csv')
            self.report_daily.to_csv(self.directory+'reportDaily'+self.end_file_name+'.csv')
            self.report_trade.to_html(self.directory+'reportTrade'+self.end_file_name+'.html')
            self.report_daily.to_html(self.directory+'reportDaily'+self.end_file_name+'.html')

    def pltPersistence(self, sl, save=False):
        fig, ax = plt.subplots(figsize=(gn*y_size, y_size))
        Persistence = self.ret_dic[sl][self.cols[1:]].mean()
        Persistence_std = self.ret_dic[sl][self.cols[1:]].std()
        Persistence.plot(ax=ax, style='o--')
        (Persistence+Persistence_std).plot(ax=ax, style='o--')
        (Persistence-Persistence_std).plot(ax=ax, style='o--')

        plt.title(self.instrument+'  PERSISTENCE', color='black', fontsize=20)
        plt.xticks(color='black')
        plt.yticks(color='black')
        plt.tick_params(axis='both', which='major', labelsize=15)
        fig.autofmt_xdate()
        plt.xlabel('VWAP Buckets', color='black', fontsize=20)
        plt.ylabel('Return Per Trade (BP)', color='black', fontsize=20)
        if save:
            fig.savefig(self.directory+'persistence.pdf', bbox_inches='tight')
            fig.savefig(self.directory+'persistence.jpeg', bbox_inches='tight')

    def pltMultiplePersistence(self):
        fig, ax = plt.subplots(figsize=(gn*y_size, y_size))
        for col, color in zip(self.sl_vec, colors):
            if col == np.inf:
                label = 'no stoploss'
            else:
                label = str(col) + 'bp stoploss'
            self.ret_dic[col][self.cols[1:]].mean().plot(ax=ax, 
                                                     color=color, 
                                                     marker='o',  
                                                     markersize=7, 
                                                     label=label)
        plt.title(self.instrument+'  PERSISTENCE', color='black', fontsize=20)
        plt.xticks(color='black')
        plt.yticks(color='black')
        plt.tick_params(axis='both', which='major', labelsize=15)
        fig.autofmt_xdate()
        plt.xlabel('VWAP Buckets', color='black', fontsize=20)
        plt.ylabel('Return Per Trade (BP)', color='black', fontsize=20)
        patches, labels = ax.get_legend_handles_labels()
        ax.legend(patches, labels, loc=2)
        if self.save:
            fig.savefig(self.directory+'persistences'+self.end_file_name+'.pdf', bbox_inches='tight')
            fig.savefig(self.directory+'persistences'+self.end_file_name+'.jpeg', bbox_inches='tight', 
                        dpi=dpi)

    def pltQuartilePerformance(self):
        fig, ax = plt.subplots(figsize=(gn*y_size, y_size))
        returns = self.ret_dic[np.inf][self.cols[-1]]
        ret_mid = np.percentile(returns, 50)
        returns_plus = returns[returns>ret_mid]
        returns_minus = returns[returns<ret_mid]
        ret_quart1 = np.percentile(returns_minus, 50)
        ret_quart3 = np.percentile(returns_plus, 50)
        print(ret_quart1, ret_mid, ret_quart3)
        indx4 = (returns<ret_quart1).values
        indx3 = ((returns>ret_quart1) & (returns<ret_mid)).values
        indx2 = ((returns>ret_mid) & (returns<ret_quart3)).values
        indx1 = (returns>ret_quart3).values
        persitency1 = self.ret_dic[np.inf][self.cols[1:]][indx1].mean()
        persitency2 = self.ret_dic[np.inf][self.cols[1:]][indx2].mean()
        persitency3 = self.ret_dic[np.inf][self.cols[1:]][indx3].mean()
        persitency4 = self.ret_dic[np.inf][self.cols[1:]][indx4].mean()
        persitency1.plot(color=colors[3], marker='o',  markersize=7, label='Q4', legend=False)
        persitency2.plot(color=colors[2], marker='o',  markersize=7, label='Q3', legend=False)
        persitency3.plot(color=colors[1], marker='o',  markersize=7, label='Q2', legend=False)
        persitency4.plot(color=colors[0], marker='o',  markersize=7, label='Q1', legend=False)
        patches, labels = ax.get_legend_handles_labels()
        ax.legend(patches, labels, loc='best')
        plt.xticks(color='black')
        plt.yticks(color='black')
        plt.title(self.instrument+'  QUARTILE PERFORMANCE', color='black', fontsize=20)
        plt.tick_params(axis='both', which='major', labelsize=15)
        fig.autofmt_xdate()
        plt.xlabel('VWAP Buckets', color='black', fontsize=20)
        plt.ylabel('Return Per Trade (BP)', color='black', fontsize=20)
        if self.save:
            fig.savefig(self.directory+'quartiles'+self.end_file_name+'.pdf', bbox_inches='tight')
            fig.savefig(self.directory+'quartiles'+self.end_file_name+'.jpeg', bbox_inches='tight', 
                        dpi=dpi)

    def pltQuartileProbaSuccess(self):
        fig, ax = plt.subplots(figsize=(gn*y_size, y_size))
        returns = self.ret_dic[np.inf][self.cols[-1]]
        ret_mid = np.percentile(returns, 50)
        returns_plus = returns[returns>ret_mid]
        returns_minus = returns[returns<ret_mid]
        ret_quart1 = np.percentile(returns_minus, 50)
        ret_quart3 = np.percentile(returns_plus, 50)
        print(ret_quart1, ret_mid, ret_quart3)
        indx4 = (returns<ret_quart1).values
        indx3 = ((returns>ret_quart1) & (returns<ret_mid)).values
        indx2 = ((returns>ret_mid) & (returns<ret_quart3)).values
        indx1 = (returns>ret_quart3).values
        persitency1 = self.ret_dic[np.inf][self.cols[1:]][indx1].mean()
        persitency2 = self.ret_dic[np.inf][self.cols[1:]][indx2].mean()
        persitency3 = self.ret_dic[np.inf][self.cols[1:]][indx3].mean()
        persitency4 = self.ret_dic[np.inf][self.cols[1:]][indx4].mean()
        persitency1.plot(color=colors[3], marker='o',  markersize=7, label='Q4', legend=False)
        persitency2.plot(color=colors[2], marker='o',  markersize=7, label='Q3', legend=False)
        persitency3.plot(color=colors[1], marker='o',  markersize=7, label='Q2', legend=False)
        persitency4.plot(color=colors[0], marker='o',  markersize=7, label='Q1', legend=False)
        patches, labels = ax.get_legend_handles_labels()
        ax.legend(patches, labels, loc='best')
        plt.xticks(color='black')
        plt.yticks(color='black')
        plt.title(self.instrument+'  QUARTILE PERFORMANCE', color='black', fontsize=20)
        plt.tick_params(axis='both', which='major', labelsize=15)
        fig.autofmt_xdate()
        plt.xlabel('VWAP Buckets', color='black', fontsize=20)
        plt.ylabel('Return Per Trade (BP)', color='black', fontsize=20)
        if self.save:
            fig.savefig(self.directory+'quartiles'+self.end_file_name+'.pdf', bbox_inches='tight')
            fig.savefig(self.directory+'quartiles'+self.end_file_name+'.jpeg', bbox_inches='tight', 
                        dpi=dpi)

    def pltQuartileScore(self):
        fig, ax = plt.subplots(figsize=(gn*y_size, y_size))
        returns = self.ret_dic[np.inf][self.cols[-1]]
        ret_mid = np.percentile(returns, 50)
        returns_plus = returns[returns>ret_mid]
        returns_minus = returns[returns<ret_mid]
        ret_quart1 = np.percentile(returns_minus, 50)
        ret_quart3 = np.percentile(returns_plus, 50)
        print(ret_quart1, ret_mid, ret_quart3)
        indx4 = (returns<ret_quart1).values
        indx3 = ((returns>ret_quart1) & (returns<ret_mid)).values
        indx2 = ((returns>ret_mid) & (returns<ret_quart3)).values
        indx1 = (returns>ret_quart3).values
        persitency1 = self.ret_dic[np.inf][self.cols[1:]][indx1].mean()
        persitency2 = self.ret_dic[np.inf][self.cols[1:]][indx2].mean()
        persitency3 = self.ret_dic[np.inf][self.cols[1:]][indx3].mean()
        persitency4 = self.ret_dic[np.inf][self.cols[1:]][indx4].mean()
        persitency1.plot(color=colors[3], marker='o',  markersize=7, label='Q4', legend=False)
        persitency2.plot(color=colors[2], marker='o',  markersize=7, label='Q3', legend=False)
        persitency3.plot(color=colors[1], marker='o',  markersize=7, label='Q2', legend=False)
        persitency4.plot(color=colors[0], marker='o',  markersize=7, label='Q1', legend=False)
        patches, labels = ax.get_legend_handles_labels()
        ax.legend(patches, labels, loc='best')
        plt.xticks(color='black')
        plt.yticks(color='black')
        plt.title(self.instrument+'  QUARTILE PERFORMANCE', color='black', fontsize=20)
        plt.tick_params(axis='both', which='major', labelsize=15)
        fig.autofmt_xdate()
        plt.xlabel('VWAP Buckets', color='black', fontsize=20)
        plt.ylabel('Return Per Trade (BP)', color='black', fontsize=20)
        if self.save:
            fig.savefig(self.directory+'quartiles'+self.end_file_name+'.pdf', bbox_inches='tight')
            fig.savefig(self.directory+'quartiles'+self.end_file_name+'.jpeg', bbox_inches='tight', 
                        dpi=dpi)

    def pltMultiplePNL(self):
        colors_ = colors
        fig, ax1 = plt.subplots(figsize=(gn*y_size, y_size))
        ax1.set_ylabel('Cumulative Return (%)', color='black', fontsize=20)
        ax1.set_xlabel('Date', color='black', fontsize=20)
        ax2 = ax1.twinx()
        for key, color in zip(self.sl_vec, colors_pnl):
            if key == np.inf:
                label = 'no stoploss'
            else:
                label = str(key) + 'bp stoploss'
            self.ret_daily_dic[key]['Ret(%)'].cumsum().plot(ax=ax1, 
                                                             style=color, 
                                                             label=label, 
                                                             linewidth=5.0)
        self.ret_daily_dic[self.sl_vec[0]]['Trades'].plot(ax=ax2, 
                                                          color='#3f8e3d',
                                                          marker='o', 
                                                          markersize=4,
                                                        linestyle='--', 
                                                          linewidth=1.5
                                                        )
        ax2.set_ylabel('Trades number', color='black', fontsize=20)

        plt.title(self.instrument+'   CUMULATIVE RETURN', color='black', fontsize=20)
        for label in ax1.get_yticklabels():
            label.set_color('black')
            label.set_fontsize(fs)
        for label in ax1.xaxis.get_majorticklabels():
            label.set_color('black')
            label.set_fontsize(fs)
        for label in ax2.yaxis.get_majorticklabels():
            label.set_color('black')
            label.set_fontsize(fs)
        patches, labels = ax1.get_legend_handles_labels()
        ax1.legend(patches, labels, loc='best')
        patches, labels = ax2.get_legend_handles_labels()
        ax2.legend(patches, labels, loc=4)
        plt.tick_params(axis='both', which='major', labelsize=15)
        fig.autofmt_xdate()
        if self.save:
            fig.savefig(self.directory+'cumulativeReturn'+self.end_file_name+'.pdf', bbox_inches='tight')
            fig.savefig(self.directory+'cumulativeReturn'+self.end_file_name+'.jpeg', bbox_inches='tight', 
                        dpi=dpi)

    def pltStoplossVSReturn(self, minutes_shift=0):
        sl_vec = np.arange(1, self.sl_max+1)
        n_sl = len(sl_vec)
        bps = np.zeros(n_sl)
        rets = np.zeros(n_sl)
        ratios = np.zeros(n_sl)
        for j, sl in enumerate(sl_vec):
            full_index = np.ones(self.n_trades, bool)
            previous_index = np.ones(self.n_trades, bool)
            ret_sl = 0
            N = 0
            for i in range(self.n_buckets):
                a = (self.ret_mat[:, i] > -sl)
                previous_index = full_index.copy()
                full_index *= a
                try:
                    ret_sl += self.ret_mat[:, i+minutes_shift][full_index != previous_index].sum()
                except:
                    ret_sl += self.ret_mat[:, -1][full_index != previous_index].sum()
                N += (full_index != previous_index).sum()
            ratios[j] = 100*(1-full_index.sum()/len(full_index))
            rets[j] = (ret_sl+self.ret_mat[full_index, -1].sum()) / 100
            bps[j] = (ret_sl+self.ret_mat[full_index, -1].sum())/self.n_trades
        print(self.proba_success, self.threshold_secs, self.max_trades)
        print('Max Return per Trade: stoploss ', 
              sl_vec[np.argmax(bps)], ', return ',
              round(np.max(bps), 2), 'bp', sep='')
        fig, ax1 = plt.subplots(figsize=(gn*y_size, y_size))
        ax2 = ax1.twinx()
        ax1.set_title(self.instrument+'  RETURN PER TRADE vs STOPLOSS', color='black', fontsize=20)
        ax1.set_xlabel('stoploss (BP)', color='black', fontsize=20)
        ax1.set_xlim(0, self.sl_max+1)
        ax1.set_ylabel('return per trade (BP)', color='black', fontsize=20)
        ax2.set_ylabel('stoploss triggered (%)', color='black', fontsize=20)
        plt.tick_params(axis='both', which='major', labelsize=15)
        plt.tight_layout()
        ax1.plot(sl_vec, bps,'o--', label='return per trade');
        ax2.plot(sl_vec, ratios,'or--', label='stoploss triggered');
        for label in ax1.get_yticklabels():
            label.set_color('black')
            label.set_fontsize(fs)
        for label in ax1.xaxis.get_majorticklabels():
            label.set_color('black')
            label.set_fontsize(fs)
        for label in ax2.yaxis.get_majorticklabels():
            label.set_color('black')
            label.set_fontsize(fs)
        patches, labels = ax1.get_legend_handles_labels()
        ax1.legend(patches, labels, loc=1)
        patches, labels = ax2.get_legend_handles_labels()
        ax2.legend(patches, labels, loc=3)
        plt.tick_params(axis='both', which='major', labelsize=15)
        fig.autofmt_xdate()
        if self.save:
            fig.savefig(self.directory+'stoplossReturn'+self.end_file_name+'.jpeg', bbox_inches='tight', 
                        dpi=dpi)
            fig.savefig(self.directory+'stoplossReturn'+self.end_file_name+'.pdf', bbox_inches='tight', 
                        dpi=dpi)            

    def getLeonData(self, sl, delta_vwap, file_name):

        if self.filters == True:
            file_name += 'filter_'
        else:
            file_name += 'pure_'
        file_name += str(self.holding_period) + 'mins_'
        if sl <1000:
            file_name += str(sl) + 'stoploss_'
        if self.proba_success >50:
            file_name += str(self.proba_success) + '%'
        file_name += '.csv'
        df = self.df_price_dic[sl].copy()
        df['Date'] = df['DateTime'].dt.date
        df['Time'] = df['DateTime'].dt.time
        df.sort_values('DateTime', inplace=True)
        df.drop('DateTime', axis=1, inplace=True)
        df.index = np.arange(len(df))
        cols_vwap = ['vwap_'+str(i) for i in [2]+list(range(5, self.holding_period+1, delta_vwap))]
        df = df[['Date', 'Time', 'Stock', 'Action', 'AnomalyPrice']+cols_vwap]
        df.to_csv(file_name)




    # def __getTrades(self):
    #     self.df_res_dic = {}
    #     df_res = pd.DataFrame(self.ret_mat, index=range(0, self.n_trades), columns=self.cols)
    #     for sl in self.sl_vec:
    #         df_res_sl = df_res.copy()
    #         for i in df_res_sl.index:
    #             indx = np.argwhere(df_res_sl.loc[i] < -sl)
    #             if indx.sum():
    #                 if indx.sum() > 1:
    #                     df_res_sl.loc[i][indx[0][0]+1:] = df_res_sl.loc[i][indx[0][0]] * np.ones(self.n_buckets-indx[0][0]-1)
    #                 else: 
    #                     df_res_sl.loc[i][indx[0][0]+1:] = df_res_sl.loc[i][indx[0][0]]
    #         self.df_res_dic[sl] = df_res_sl


    # def getMatrixReturnOpeningTime(self, opening_time, seed=0):
    #     if opening_time == 0:
    #         rets = 100 * 100 * (self.df_price[self.cols].values / 
    #                             self.df_price[self.cols[0]].values.reshape(self.n_trades, 1) - 1)
    #     else:
    #         column =  'vwap_' + str(opening_time+4)
    #         rets = 100 * 100 * (self.df_price[self.cols[opening_time:]].values / 
    #                             self.df_price[column].values.reshape(self.n_trades, 1) - 1)
    #     self.ret_mat = rets * self.df_price['Action'].values.reshape(self.n_trades, 1)

    # def getTradeReturns(self, minutes_shift=0):
    #     df_ret_dic = []
    #     for sl in self.sl_vec: 
    #         full_index = np.ones(self.n_trades, bool)
    #         previous_index = np.ones(self.n_trades, bool)
    #         ret_sl = 0
    #         N = 0
    #         returns = pd.DataFrame(columns=['Ret', 'Stock'], index=self.df_price.index)
    #         returns['Stock'] = self.df_price['Stock']
    #         for i in range(self.n_buckets):
    #             a = (self.ret_mat[:, i] > -sl)
    #             previous_index = full_index.copy()
    #             full_index *= a
    #             try:
    #                 ret_sl += self.ret_mat[:, i+minutes_shift][full_index != previous_index].sum()
    #             except:
    #                 ret_sl += self.ret_mat[:, -1][full_index != previous_index].sum()
    #             returns['Ret'][full_index != previous_index] = \
    #                 self.ret_mat[:, i][full_index != previous_index]
    #             N += (full_index != previous_index).sum()
    #         if sl == np.inf:
    #             label = '  NO stoploss'
    #         else:
    #             label = str(sl) + 'bp stoploss'
    #         print(label, round((ret_sl+self.ret_mat[full_index, -1].sum())/self.n_trades, 2))
    #         returns['Ret'][full_index] = self.ret_mat[:, -1][full_index]
    #         df_ret_dic[sl] = returns
    #         self.returns_dic[sl] = returns.copy()


    # def pltOpeningTimeVSReturn(self, n_times):
    #     opening_vec = np.arange(0, n_times+1)
    #     n_sl = len(opening_vec)
    #     bps = np.zeros(n_sl)
    #     rets = np.zeros(n_sl)
    #     ratios = np.zeros(n_sl)
    #     for j, opening_time in enumerate(opening_vec):
    #         self.getMatrixReturnOpeningTime(opening_time=opening_time)
    #         bps[j] = (self.ret_mat[:, -1].sum())/self.n_trades
    #     fig, ax = plt.subplots(figsize=(gn*y_size, y_size))
    #     ax.set_xlabel('opening time', color='r', fontsize=20)
    #     ax.set_ylabel('return per trade (BP)', color='r', fontsize=20)
    #     plt.xticks(color='black')
    #     plt.yticks(color='black')
    #     plt.tick_params(axis='both', which='major', labelsize=15)
    #     plt.tight_layout()
    #     plt.plot(opening_vec, bps,'o--');
    #     if self.save:
    #         fig.savefig(self.directory+'openingTimeReturn'+self.end_file_name+'.pdf', bbox_inches='tight')
    #         fig.savefig(self.directory+'openingTimeReturn'+self.end_file_name+'.jpeg', bbox_inches='tight', 
    #                     dpi=dpi)

    # def pltOpeningTimeVSReturnStopLoss(self, n_times, sl):
    #     opening_vec = np.arange(0, n_times+1)
    #     n_sl = len(opening_vec)
    #     bps = np.zeros(n_sl)
    #     rets = np.zeros(n_sl)
    #     ratios = np.zeros(n_sl)
    #     for j, opening_time in enumerate(opening_vec):
    #         self.getMatrixReturnOpeningTime(opening_time=opening_time)
    #         full_index = np.ones(self.n_trades, bool)
    #         previous_index = np.ones(self.n_trades, bool)
    #         ret_sl = 0
    #         N = 0
    #         for i in range(self.holding_period-n_times):
    #             a = (self.ret_mat[:, i] > -sl)
    #             previous_index = full_index.copy()
    #             full_index *= a
    #             ret_sl += self.ret_mat[:, i][full_index != previous_index].sum()
    #             N += (full_index != previous_index).sum()
    #         bps[j] = (ret_sl+self.ret_mat[full_index, -1].sum()) / self.n_trades
    #     fig, ax = plt.subplots(figsize=(gn*y_size, y_size))
    #     ax.set_xlabel('opening time', color='r', fontsize=20)
    #     ax.set_ylabel('return per trade (BP)', color='r', fontsize=20)
    #     plt.xticks(color='black')
    #     plt.yticks(color='black')
    #     plt.tick_params(axis='both', which='major', labelsize=15)
    #     plt.tight_layout()
    #     plt.plot(opening_vec, bps,'o--')
    #     if self.save:
    #         fig.savefig(self.directory+'stoploss_ret'+self.end_file_name+'.pdf', bbox_inches='tight')
    #         fig.savefig(self.directory+'stoploss_ret'+self.end_file_name+'.jpeg', bbox_inches='tight', 
    #                     dpi=dpi)


    # def pltOpeningTimeVSReturnStopLoss2D(self, opening_vec, sl_vec):
    #     n_sl = len(sl_vec)
    #     n_opening = len(opening_vec)
    #     bps = np.zeros((n_opening, n_sl))
    #     for j, opening_time in enumerate(opening_vec):
    #         self.getMatrixReturnOpeningTime(opening_time=opening_time)
    #         for k, sl in enumerate(sl_vec):
    #             full_index = np.ones(self.n_trades, bool)
    #             previous_index = np.ones(self.n_trades, bool)
    #             ret_sl = 0
    #             N = 0
    #             for i in range(self.n_buckets-n_opening):
    #                 a = (self.ret_mat[:, i] > -sl)
    #                 previous_index = full_index.copy()
    #                 full_index *= a
    #                 ret_sl += self.ret_mat[:, i][full_index != previous_index].sum()
    #                 N += (full_index != previous_index).sum()
    #             bps[j, k] = (ret_sl+self.ret_mat[full_index, -1].sum()) / self.n_trades
    #     fig, ax = plt.subplots(figsize=(gn*y_size, y_size))
    #     ax.set_xlabel('opening time', color='r', fontsize=20)
    #     ax.set_ylabel('stoploss (BP)', color='r', fontsize=20)
    #     ax.set_title('RETURN PER TRADE (BP)', color='r', fontsize=20)
    #     plt.xticks(color='black')
    #     plt.yticks(color='black')
    #     plt.tick_params(axis='both', which='major', labelsize=15)
    #     ax.set_xlim(opening_vec[0], opening_vec[-1])
    #     ax.set_xticks(np.arange(1.5, 1.5+n_sl))
    #     ax.set_yticks(0.25*sl_vec[0]+sl_vec)
    #     xtics_str = []
    #     for i in np.arange(n_opening):
    #         xtics_str.append('vwap_'+str(i+4))
    #     ytics_str = []
    #     for sl in sl_vec:
    #         ytics_str.append(str(sl))
    #     ax.set_xticklabels(xtics_str)
    #     ax.set_yticklabels(ytics_str)
    #     ax.set_ylim(sl_vec[0], sl_vec[-1])
    #     X,Y = np.meshgrid(opening_vec, sl_vec)
    #     im = plt.pcolor(X, Y, bps.T, cmap=cmap, vmin=abs(bps).min(), vmax=abs(bps).max())
    #     cb = fig.colorbar(im)
    #     plt.tight_layout()
    #     fig.autofmt_xdate()
    #     for label in cb.ax.get_yticklabels():
    #         label.set_color('r')
    #         label.set_fontsize(fs)
    #     if self.save:
    #         fig.savefig(self.directory+'stoploss2d_opening.pdf', bbox_inches='tight')
    #         fig.savefig(self.directory+'stoploss2d_opening.jpeg', bbox_inches='tight', 
    #                     dpi=dpi)

    # def pltClosingTimeVSReturnStopLoss2D(self, closing_vec, sl_vec):
    #     n_sl = len(sl_vec)
    #     n_closing = len(closing_vec)
    #     bps = np.zeros((n_closing, n_sl))
    #     self.getMatrixReturnOpeningTime(opening_time=0)
    #     for j, closing_time in enumerate(closing_vec):
    #         for k, sl in enumerate(sl_vec):
    #             full_index = np.ones(self.n_trades, bool)
    #             previous_index = np.ones(self.n_trades, bool)
    #             ret_sl = 0
    #             N = 0
    #             for i in range(self.n_buckets):
    #                 a = (self.ret_mat[:, i] > -sl)
    #                 previous_index = full_index.copy()
    #                 full_index *= a
    #                 try:
    #                     ret_sl += self.ret_mat[:, i+closing_time][full_index != previous_index].sum()
    #                 except:
    #                     ret_sl += self.ret_mat[:, -1][full_index != previous_index].sum()
    #                 N += (full_index != previous_index).sum()
    #             bps[j, k] = (ret_sl+self.ret_mat[full_index, -1].sum()) / self.n_trades
    #     fig, ax = plt.subplots(figsize=(gn*y_size, y_size))
    #     ax.set_xlabel('closing time', color='r', fontsize=20)
    #     ax.set_ylabel('stoploss (BP)', color='r', fontsize=20)
    #     ax.set_title('RETURN PER TRADE (BP)', color='r', fontsize=20)
    #     plt.xticks(color='black')
    #     plt.yticks(color='black')
    #     plt.tick_params(axis='both', which='major', labelsize=15)
    #     ax.set_xlim(closing_vec[0], closing_vec[-1])
    #     ax.set_xticks(0.5+np.arange(closing_vec[0], closing_vec[-1], 1))
    #     ax.set_yticks(0.25*sl_vec[0]+sl_vec)
    #     xtics_str = []
    #     for i in np.arange(closing_vec[0], closing_vec[-1], 1):
    #         xtics_str.append('vwap_'+str(i))
    #     ytics_str = []
    #     for sl in sl_vec:
    #         ytics_str.append(str(sl))
    #     ax.set_xticklabels(xtics_str)
    #     ax.set_yticklabels(ytics_str)
    #     ax.set_ylim(sl_vec[0], sl_vec[-1])
    #     X,Y = np.meshgrid(closing_vec, sl_vec)
    #     im = plt.pcolor(X, Y, bps.T, cmap=cmap, vmin=abs(bps).min(), vmax=abs(bps).max())
        
    #     cb = fig.colorbar(im)
    #     plt.tight_layout()
    #     fig.autofmt_xdate()
    #     plt.xlim(0, n_closing-1.)
    #     for label in cb.ax.get_yticklabels():
    #         label.set_color('r')
    #         label.set_fontsize(fs)
    #     if self.save:
    #         fig.savefig(self.directory+'stoploss2d_closing.pdf', bbox_inches='tight')
    #         fig.savefig(self.directory+'stoploss2d_closing.jpeg', bbox_inches='tight', 
    #                     dpi=dpi)

    # def pltDailyReturn(self, key, save=False):
    #     fig, ax = plt.subplots(figsize=(15, 5)) 
    #     plt.bar(self.returns_mins_sl.columns, self.returns_mins_sl.max(), width=1, color='b')
    #     plt.bar(self.returns_mins_sl.columns, self.returns_mins_sl.min(), width=1, color='r')
    #     plt.title(self.instrument+'  DAILY RETURNS', color='black', fontsize=20)
    #     plt.xticks(color='black')
    #     plt.yticks(color='black')
    #     plt.tick_params(axis='both', which='major', labelsize=15)
    #     fig.autofmt_xdate()
    #     plt.ylabel('Daily Return (%)', color='black', fontsize=20)
    #     patches, labels = ax.get_legend_handles_labels()
    #     ax.legend(patches, labels, loc=2)
    #     if save:
    #         fig.savefig(self.directory+'dailyReturn'+key+'.pdf', bbox_inches='tight')
    #         fig.savefig(self.directory+'dailyReturn'+key+'.jpeg', bbox_inches='tight')


    # def pltMultipleTradeReturnHistogram(self, y_lim=75000, x_lim=100, bins=30, save=False):
    #     fig, ax = plt.subplots(1,len(self.sl_vec),  figsize=(3*len(self.sl_vec), 6), sharey=True)
    #     fig.suptitle('HISTOGRAM RETURN per TRADE', fontsize=20, color='r')
    #     bins = np.linspace(-x_lim, x_lim, bins)
    #     for i, sl in enumerate(self.sl_vec):
    #         ax[i].set_ylim(0, y_lim)
    #         ax[i].set_xlim(-x_lim, x_lim)
    #         if sl == np.inf:
    #             title = 'no stoploss'
    #         else:
    #             title = str(sl) +  'bp stoploss'
    #         ax[i].set_title(title, color='r', fontsize=12)
    #         self.returns_dic[sl]['Ret'].hist(bins=bins, ax=ax[i], histtype='stepfilled')
    #         for label in ax[i].get_yticklabels():
    #             label.set_color('r')
    #             label.set_fontsize(fs)
    #         for label in ax[i].get_xticklabels():
    #             label.set_color('r')
    #             label.set_fontsize(fs)
    #     plt.tight_layout()
    #     fig.autofmt_xdate()
    #     plt.subplots_adjust(top=0.85, hspace=0)
    #     if save:
    #         fig.savefig(self.directory+'trades_hist'+self.session+'.pdf', bbox_inches='tight')
    #         fig.savefig(self.directory+'trades_hist'+self.session+'.jpeg', bbox_inches='tight')

    # def pltMultipleDailyReturnHistogram(self, save=False, y_lim=100, x_lim=500, bins=80):
    #     fig, ax = plt.subplots(1,len(self.sl_vec),  figsize=(3*len(self.sl_vec), 6), sharey=True)
    #     fig.suptitle('HISTOGRAM RETURN per DAY', fontsize=20, color='r')
    #     bins = np.linspace(-x_lim, x_lim, bins)
    #     for i, sl in enumerate(self.sl_vec):
    #         ax[i].set_ylim(0, y_lim)
    #         ax[i].set_xlim(-x_lim, x_lim)
    #         if sl == np.inf:
    #             title = 'no stoploss'
    #         else:
    #             title = str(sl) +  'bp stoploss'
    #         ax[i].set_title(title, color='r', fontsize=12)
    #         ax[i].plot([0, 0], [0, y_lim], 'g')
    #         self.returns_daily_dic[sl]['Ret'].hist(bins=bins, ax=ax[i], histtype='stepfilled')
    #         for label in ax[i].get_yticklabels():
    #             label.set_color('r')
    #             label.set_fontsize(fs)
    #         for label in ax[i].get_xticklabels():
    #             label.set_color('r')
    #             label.set_fontsize(fs)
    #     plt.tight_layout()
    #     fig.autofmt_xdate()
    #     plt.subplots_adjust(top=0.85, hspace=0)
    #     if save:
    #         fig.savefig(self.directory+'daily_hist'+self.session+'.pdf', bbox_inches='tight')
    #         fig.savefig(self.directory+'daily_hist'+self.session+'.jpeg', bbox_inches='tight')
